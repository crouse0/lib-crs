#include <stdio.h>
#include <stdlib.h>

#ifndef BAG_H
#define BAG_H

typedef struct BagNode {
	int item;
	struct BagNode *next;
} BagNode;

typedef struct Bag {
	BagNode *first;
	int n;
} Bag;

// allocates and initializes a new Bag Node
static BagNode *BagNode_init(int item)
{
	BagNode *n;

	if ((n = (BagNode *)malloc(sizeof(BagNode))) == NULL) {
		fprintf(stderr, "Warning: invalid malloc().\n");
		return NULL;
	}

	n->item = item;
	n->next = NULL;
	return n;
}

/* Bag_init:  allocates and initializes a new Bag
 *
 *  Returns:
 *      Bag * -- the new Bag
 */
Bag *Bag_init()
{
	Bag *b;

	if ((b = (Bag *)malloc(sizeof(Bag))) == NULL) {
		fprintf(stderr, "Warning: invalid malloc().\n");
		return NULL;
	}

	b->first = NULL;
	b->n = 0;
	return b;
}

/* Bag_add:  adds a new item to the Bag
 *
 *  Arguments:
 *      Bag *b -- a pointer to a Bag
 *      int item -- integer to add to the Bag
 */
void Bag_add(Bag *b, int item)
{
	BagNode *n = BagNode_init(item);
	n->next = b->first;
	b->first = n;
	b->n++;
}

/* Bag_print:  iterates through a Bag
 *
 *  Arguments:
 *      Bag *b -- a pointer to a Bag
 */
void Bag_print(Bag *b)
{
	BagNode *n;

	for (n = b->first; n != NULL; n = n->next) {
		printf("%d%s", n->item, n->next == NULL ? "" : ", ");
	}
}

/* Bag_free:  frees all allocated resources for a given Bag
 *
 *  Arguments:
 *      Bag *b -- a pointer to a Bag
 */
void Bag_free(Bag *b)
{
	BagNode *itr;

	while ((itr = b->first) != NULL) {
		b->first = b->first->next;
		free(itr);
		itr = NULL;
	}

	free(b);
}

#endif