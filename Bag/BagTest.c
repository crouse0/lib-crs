#include "../STDIN.h"
#include "Bag.h"

// tests for Bag.h
int main(int argc, char *argv[])
{
	Bag *b = Bag_init();

	while (!STDIN_isEmpty()) {
		int d = STDIN_readInt();
		Bag_add(b, d);
	}

	printf("Length of Bag: %d\n", b->n);
	printf("Items in Bag:\t");
	Bag_print(b);
	printf("\n");
	Bag_free(b);
	return 0;
}
