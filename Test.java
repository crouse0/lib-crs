import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.printf("Usage: java Test [-s/-l/-a]\n");
			System.exit(1);
		}

		if (args[0].equals("-s")) {
			while (!StdIn.isEmpty()) {
				StdOut.printf("|%s|\n", StdIn.readString());
			}
		} else if (args[0].equals("-l")) {
			while (!StdIn.isEmpty()) {
				StdOut.printf("|%s|\n", StdIn.readLine());
			}
		} else if (args[0].equals("-a")) {
			StdOut.printf("%s", StdIn.readAll());
		} else {
			System.err.printf("Error: invalid flag.\n");
			System.exit(1);
		}
	}
}