#include "../in.h"
#include "../fin.h"
#include "Graph.h"
#include "GraphDFS.h"

// tests for GraphDFS.h
int main(int argc, char const *argv[])
{
	// create a new graph
	if (argc < 2) {
		fprintf(stderr, "Usage: %s <graph_inp>\n", *argv);
		exit(1);
	}

	FILE *fp = fopen(argv[1], "r");
	validateFile(fp);
	Graph *g = Graph_file_init(fp);
	int s, i;
	s = in_read_int(); // point to stage dfs from
	GraphDFS *dfs = GraphDFS_init(g, s);
	printf("executing dfs from vertice %d on graph:\n", s);
	Graph_print(g);
	printf("\nvertices reachable from %d:\n", s);

	for (i = 0; i < g->V; i++) {
		if (dfs->marked[i]) {
			printf("%d ", i);
		}
	}

	printf("\n");
	printf("The graph is ");

	if (dfs->count != g->V) {
		printf("not connected.\n");
	} else {
		printf("connected.\n");
	}

	GraphDFS_free(dfs);
	Graph_free(g);
	fclose(fp);
	return 0;
}