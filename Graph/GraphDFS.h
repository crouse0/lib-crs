#include "Graph.h"

#ifndef GRAPHDFS_H
#define GRAPHDFS_H

typedef struct GraphDFS {
	int vertex;  // the source vertex
	int *marked; // array[i] == 1 if connected, and
	int n;       // length of "marked" i.e. number of vertices in the graph
	int count;   // number of vertices connected to "vertex"
} GraphDFS;

// validates that "v" is a valid vertex in "g"
extern void validateVertex(Graph *g, int v);

// performs a recursive dfs on g from v, and stores the results in "dfs"
void GraphDFS_dfs(GraphDFS *dfs, Graph *g, int v)
{
	validateVertex(g, v);
	dfs->count++;
	dfs->marked[v] = 1;
	int w;
	BagNode *p;

	for (p = g->adj[v]->first; p != NULL; p = p->next) {
		w = p->item;

		if (!dfs->marked[w]) {
			GraphDFS_dfs(dfs, g, w);
		}
	}
}

/* GraphDFS_init:  allocates and initializes a new GraphDFS, then performs a
 *  Depth First Search on the given Graph to compute the connected vertices
 *  of the given vertice.
 *
 *  Arguments:
 *      Graph *g -- Graph to perform DFS on
 *      int v -- source vertex
 *
 *  Returns:
 *      GraphDFS * -- a pointer to the DFS results
 */
GraphDFS *GraphDFS_init(Graph *g, int v)
{
	validateVertex(g, v);
	GraphDFS *dfs;

	if ((dfs = (GraphDFS *)malloc(sizeof(GraphDFS))) == NULL) {
		fprintf(stderr, "Error: invalid call to malloc().\n");
		exit(1);
	}

	dfs->vertex = v;
	dfs->n = g->V;

	if ((dfs->marked = (int *)malloc(sizeof(int) * dfs->n)) == NULL) {
		// could not allocate memory
		fprintf(stderr, "Error: invalid malloc().\n");
		exit(1);
	}

	int i;

	for (i = 0; i < dfs->n; i++) {
		// initializes the marked array
		dfs->marked[i] = 0;
	}

	dfs->count = 0;
	GraphDFS_dfs(dfs, g, dfs->vertex);
	return dfs;
}

/* GraphDFS_free:  frees all allocated resources for the DFS,
 *  but does not free the internal graph
 *
 *  Arguments:
 *      GraphDFS *dfs -- the dfs to free
 */
void GraphDFS_free(GraphDFS *dfs)
{
	free(dfs->marked);
	dfs->marked = NULL;
	free(dfs);
	dfs = NULL;
}

#endif