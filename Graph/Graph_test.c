#include "../in.h"
#include "Graph.h"

// tests for Graph.h
int main(int argc, char *argv[])
{
	int V, E, i, v, w;
	V = in_read_int();
	E = in_read_int();

	if (V < 0 || E < 0) {
		fprintf(stderr, "Error: invalid number of vertices or edges.\n");
		exit(1);
	}

	Graph *g = Graph_init(V);

	while (in_has_next()) {
		v = in_read_int();
		w = in_read_int();
		validateVertex(g, v);
		validateVertex(g, w);
		Graph_addEdge(g, v, w);
	}

	Graph_print(g);
	Graph_free(g);
	return 0;
}
