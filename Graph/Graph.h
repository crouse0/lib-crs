#include <stdio.h>
#include <stdlib.h>

#include "../fin.h"
#include "../Bag/Bag.h"

#ifndef GRAPH_H
#define GRAPH_H

typedef struct Graph {
	int V;     // number of vertices
	int E;     // number of edges in the graph
	Bag **adj; // singly-linked-list of adjacent vertices
} Graph;

/* Graph_init:  allocates and initializes memory for a new Graph
 *
 *  Arguments:
 *      int V -- number of vertices
 *
 *  Returns:
 *      Graph * -- the new Graph
 */
Graph *Graph_init(int V)
{
	Graph *g;
	int i;

	if ((g = (Graph *)malloc(sizeof(Graph))) == NULL) {
		// couldn't allocate the requested memory
		fprintf(stderr, "Error: invalid call to malloc().\n");
		exit(1);
	}

	if ((g->adj = (Bag **)malloc(sizeof(Bag *) * V)) == NULL) {
		fprintf(stderr, "Error: invalid call to malloc().\n");
		exit(1);
	}

	for (i = 0; i < V; i++) {
		// initialize the adjacency lists
		g->adj[i] = Bag_init();
	}

	g->V = V;
	g->E = 0;
	return g;
}

// validates that the given vertex "v" is in "g"
static void validateVertex(Graph *g, int v)
{
	if (v < 0 || v >= g->V) {
		// "v" is invalid
		fprintf(stderr, "Error: vertex is not between 0 and %d.\n", g->V - 1);
		exit(1);
	}
}

/* Graph_add_edge:  adds an edge between "v" and "w"
 *
 *  Arguments:
 *      Graph *g -- a pointer to a Graph
 *      int v -- the first vertice
 *      int w -- the second vertice
 */
void Graph_add_edge(Graph *g, int v, int w)
{
	validateVertex(g, v), validateVertex(g, w);
	g->E++;
	Bag_add(g->adj[v], w);
	Bag_add(g->adj[w], v);
}

/* Graph_file_init:  creates a new graph then inserts the data from the
 *  given file into the Graph
 *
 *  Arguments:
 *      FILE *fp -- a pointer to a file
 *
 *  Returns:
 *      Graph * -- the new graph
 */
Graph *Graph_file_init(FILE *fp)
{
	validateFile(fp);
	int V, E, i, v, w;
	V = fin_read_int(fp);
	E = fin_read_int(fp);

	if (V < 0 || E < 0) {
		fprintf(stderr, "Error: invalid number of vertices or edges.\n");
		exit(1);
	}

	Graph *g = Graph_init(V);

	while (fin_has_next(fp)) {
		v = fin_read_int(fp);
		w = fin_read_int(fp);
		validateVertex(g, v);
		validateVertex(g, w);
		Graph_add_edge(g, v, w);
	}

	return g;
}

/* Graph_vertex_degree:  determines the number of vertices incident on "v"
 *
 *  Arguments:
 *      Graph *g -- a pointer to a Graph
 *      int v -- the vertice
 *
 *  Returns:
 *      int -- the number of vertices connected to "v"
 */
int Graph_vertex_degree(Graph *g, int v)
{
	validateVertex(g, v);
	return g->adj[v]->n;
}

/* Graph_print:  outputs a formatted representation of the given Graph
 *
 *  Arguments:
 *      Graph *g -- a pointer to a Graph
 */
void Graph_print(Graph *g)
{
	printf("%d vertices and %d edges.\n", g->V, g->E);
	int i;

	for (i = 0; i < g->V; i++) {
		printf("%d: ", i);
		Bag_print(g->adj[i]);
		printf("\n");
	}
}

/* Graph_free:  frees all allocated resources for a given Graph
 *
 *  Arguments:
 *      Graph *g -- a pointer to a Graph
 */
void Graph_free(Graph *g)
{
	int i;

	for (i = 0; i < g->V; i++) {
		Bag_free(g->adj[i]);
		g->adj[i] = NULL;
	}

	free(g->adj);
	g->adj = NULL;
	free(g);
	g = NULL;
}

#endif