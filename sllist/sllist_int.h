#include <stdlib.h>
#include <stdio.h>

#include "sllist.h"

#ifndef SLLIST_INT_H
#define SLLIST_INT_H

void sllist_int_print(sllist *sll)
{
	if (!sll) {
		printf("The list has not been initialized or has been freed.\n");
		return;
	}

	node *p;

	for (p = sll->head; p != NULL; p = p->next) {
		printf("%d%s", (int)p->data, (p->next == NULL) ? "\n" : " -> ");
	}

	if (p == sll->head) {
		printf("The list is empty.\n");
	}
}

int sllist_int_insert(sllist *sll, int data)
{
	if (!sll) {
		printf("The list has not been initialized or has been freed.\n");
		return 0;
	}

	node *tmp = node_init((void *)data);
	tmp->next = sll->head;
	sll->head = tmp;
	sll->n++;
	return 1;
}

node *sllist_int_find(sllist *sll, int target)
{
	if (!sll) {
		printf("The list has not been initialized or has been freed.\n");
		return NULL;
	}

	node *p;

	for (p = sll->head; p != NULL; p = p->next) {
		if ((int)p->data == target) {
			return p;
		}
	}

	return NULL;
}

int sllist_int_remove(sllist *sll, int target)
{
	if (!sll) {
		printf("The list has not been initialized or has been freed.\n");
		return 0;
	}

	node *p, *s;

	for (s = p = sll->head; p != NULL; s = p, p = p->next) {
		if ((int)p->data == target) {
			if (p == sll->head) {
				sll->head = sll->head->next;
			}

			s->next = p->next;
			free(p);
			p = NULL;
			sll->n--;
			return 1;
		}
	}

	return 0;
}

void sllist_int_free(sllist *sll)
{
	if (!sll) {
		printf("The list has not been initialized or has been freed.\n");
		return;
	}

	node *p, *s;
	p = sll->head;

	while ((s = p) != NULL) {
		p = p->next;
		free(s);
		s = NULL;
	}

	sll->head = NULL;
	free(sll);
	sll = NULL;
}

#endif // SLLIST_INT_H