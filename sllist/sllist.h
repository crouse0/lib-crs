#include <stdlib.h>
#include <stdio.h>

#ifndef SLLIST_H
#define SLLIST_H

typedef struct node {
	void *data;
	struct node *next;
} node;

typedef struct sllist {
	node *head;
	int n;
} sllist;

node *node_init(void *data)
{
	node *n;

	if ((n = (node *)malloc(sizeof(node))) == NULL) {
		fprintf(stderr, "Error: invalid call to malloc(). Could not alloc new node.\n");
		exit(1);
	}

	n->data = data;
	n->next = NULL;
	return n;
}

sllist *sllist_init()
{
	sllist *sll;

	if ((sll = (sllist *)malloc(sizeof(sllist))) == NULL) {
		fprintf(stderr, "Error: invalid call to malloc(). Could not alloc a new sllist.\n");
		exit(1);
	}

	sll->head = NULL;
	sll->n = 0;
	return sll;
}

#endif // SLLIST_H