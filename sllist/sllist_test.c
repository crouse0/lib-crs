#include "../in.h"
#include "../fin.h"

#include "sllist_int.h"
#include "sllist_string.h"

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Usage: %s [-i(nt)/-s(tring)]\n", *argv);
		exit(1);
	}

	char *option = argv[1];
	sllist *sll = sllist_init();

	if (!strcmp(option, "-s") || !strcmp(option, "-string")) {
		while (in_has_next()) {
			sllist_string_insert(sll, in_read_string());
		}

		sllist_string_print(sll);
		sllist_string_free(sll);
	} else if (!strcmp(option, "-i") || !strcmp(option, "-int")) {
		while (in_has_next()) {
			sllist_int_insert(sll, in_read_int());
		}

		sllist_int_print(sll);
		sllist_int_free(sll);
	} else {
		fprintf(stderr, "Usage: %s [-i(nt)/-s(tring)]\n", *argv);
	}

	return 0;
}