#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "sllist.h"

#ifndef SLLIST_STRING_H
#define SLLIST_STRING_H

void sllist_string_print(sllist *sll)
{
	if (!sll) {
		printf("The list has not been initialized or has been freed.\n");
		return;
	}

	node *p;

	for (p = sll->head; p != NULL; p = p->next) {
		printf("%s%s", (const char *)p->data, (p->next == NULL) ? "\n" : " -> ");
	}

	if (p == sll->head) {
		printf("The list is empty.\n");
	}
}

#define MAXSTRING 1000

int sllist_string_insert(sllist *sll, char *data)
{
	if (!sll) {
		printf("The list has not been initialized or has been freed.\n");
		return 0;
	}

	node *tmp = node_init((void *)strndup(data, (size_t)MAXSTRING));
	tmp->next = sll->head;
	sll->head = tmp;
	sll->n++;
	return 1;
}

node *sllist_string_find(sllist *sll, const char *target)
{
	if (!sll) {
		printf("The list has not been initialized or has been freed.\n");
		return NULL;
	}

	node *p;

	for (p = sll->head; p != NULL; p = p->next) {
		if (!strncmp((const char *)p->data, target, (size_t)MAXSTRING)) {
			return p;
		}
	}

	return NULL;
}

int sllist_string_remove(sllist *sll, const char *target)
{
	if (!sll) {
		printf("The list has not been initialized or has been freed.\n");
		return 0;
	}

	node *p, *s;

	for (s = p = sll->head; p != NULL; s = p, p = p->next) {
		if (!strncmp((const char *)p->data, target, (size_t)MAXSTRING)) {
			if (p == sll->head) {
				sll->head = sll->head->next;
			}

			s->next = p->next;
			free(p->data);
			p->data = NULL;
			free(p);
			p = NULL;
			sll->n--;
			return 1;
		}
	}

	return 0;
}

void sllist_string_free(sllist *sll)
{
	if (!sll) {
		printf("The list has not been initialized or has been freed.\n");
		return;
	}

	node *p, *s;
	p = sll->head;

	while ((s = p) != NULL) {
		p = p->next;
		free(s->data);
		s->data = NULL;
		free(s);
		s = NULL;
	}

	sll->head = NULL;
	free(sll);
	sll = NULL;
}

#endif // SLLIST_STRING_H
