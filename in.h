#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#ifndef IN_H
#define IN_H

/* in_is_empty:  determines if "stdin" is empty
 *
 *  Returns:
 *      int -- (1) "stdin" is empty or (0) "stdin" is not empty
 */
int in_is_empty()
{
	int c;
	fpos_t pos;
	fgetpos(stdin, &pos);

	while ((c = getchar()) && isspace(c))
		; // ignore white-spaces

	ungetc(c, stdin);
	fsetpos(stdin, &pos);
	return c == EOF ? 1 : 0;
}

/* in_has_next:  determines if "stdin" has more data
 *
 *  Returns:
 *      int -- (1) "stdin" has more data or (0) "stdin" is empty
 */
int in_has_next()
{
	int c;
	fpos_t pos;
	fgetpos(stdin, &pos);

	while ((c = getchar()) && isspace(c))
		; // ignore white-spaces

	ungetc(c, stdin);
	fsetpos(stdin, &pos);
	return c == EOF ? 0 : 1;
}

/* in_read_line:  returns the line read from stdin as a string, does not
 *		include the newline character
 *
 *  Returns:
 *      char * -- a pointer to the line read
 */
char *in_read_line()
{
	size_t MAXSIZE = 1000, len;
	char *lp = NULL;

	if ((len = getline(&lp, &MAXSIZE, stdin)) == 0) {
		fprintf(stderr, "Error: there is no line to read.\n");
		exit(1);
	}

	if (lp[len - 1] == '\n') {
		lp[len - 1] = 0;
	}

	return lp;
}

/* in_read_all:  returns the enter input to "stdin" as a string
 *
 *  Returns:
 *      char * -- a pointer to the stream of input read
 */
char *in_read_all()
{
	if (!in_has_next()) {
		fprintf(stderr, "Error: there is nothing to read.\n");
		exit(1);
	}

	const size_t MAXSIZE = 7000000;
	char all[MAXSIZE], *ap;
	int c, i = 0;

	while ((c = getchar()) != EOF) {
		all[i++] = c;

		if (i == MAXSIZE - 1) {
			fprintf(stderr, "wow very large.\n");
			return NULL;
		}
	}

	all[i] = 0;
	ap = all;
	return ap;
}

/* in_read_string:  returns the next string of text, using white-spaces as a delimiter
 *
 *  Returns:
 *      char * -- a pointer to the string of input read
 */
char *in_read_string()
{
	if (!in_has_next()) {
		fprintf(stderr, "Error: there is no string to read.\n");
		exit(1);
	}

	const size_t MAXSIZE = 1000;
	char s[MAXSIZE], *sp;
	fscanf(stdin, "%999s", s);
	sp = s;
	return sp;
}

/* in_read_int:  returns the next integer from "stdin"
 *
 *  Returns:
 *      int -- the integer read
 */
int in_read_int()
{
	if (!in_has_next()) {
		fprintf(stderr, "Error: there is no integer to read.\n");
		exit(1);
	}

	fpos_t pos;
	fgetpos(stdin, &pos);
	int c;

	while ((c = getchar()) && isspace(c))
		; // ignore spaces

	ungetc(c, stdin);

	if (!isdigit(c) && c != '-' && c != '.') {
		// data read is not a number
		fsetpos(stdin, &pos);
		fprintf(stderr, "Error: input mismatch, expected int.\n");
		exit(1);
	}

	fscanf(stdin, "%d", &c); // read number into c
	return c;
}

/* in_read_float:  returns the next floating point value from "stdin"
 *
 *  Returns:
 *      float -- the floating point read
 */
float in_read_float()
{
	if (!in_has_next()) {
		fprintf(stderr, "Error: there is no float to read.\n");
		exit(1);
	}

	fpos_t pos;
	fgetpos(stdin, &pos);
	int c;

	while ((c = getchar()) && isspace(c))
		; // ignore spaces

	ungetc(c, stdin);

	if (!isdigit(c) && c != '-' && c != '.') {
		// data read is not a number
		fsetpos(stdin, &pos);
		fprintf(stderr, "Error: input mismatch, expected float.\n");
		exit(1);
	}

	float f;
	fscanf(stdin, "%f", &f);
	return f;
}

/* in_read_double:  returns the next double precision floating point value from "stdin"
 *
 *  Returns:
 *      double -- the double read
 */
double in_read_double()
{
	if (!in_has_next()) {
		fprintf(stderr, "Error: there is no double to read.\n");
		exit(1);
	}

	fpos_t pos;
	fgetpos(stdin, &pos);
	int c;

	while ((c = getchar()) && isspace(c))
		; // ignore spaces

	ungetc(c, stdin);

	if (!isdigit(c) && c != '-' && c != '.') {
		// data read is not a number
		fsetpos(stdin, &pos);
		fprintf(stderr, "Error: input mismatch, expected double.\n");
		exit(1);
	}

	double d;
	fscanf(stdin, "%lf", &d);
	return d;
}

/* in_read_long:  returns the next long value from "stdin"
 *
 *  Returns:
 *      long -- the long read
 */
long in_read_long()
{
	if (!in_has_next()) {
		fprintf(stderr, "Error: there is no long to read.\n");
		exit(1);
	}

	fpos_t pos;
	fgetpos(stdin, &pos);
	int c;

	while ((c = getchar()) && isspace(c))
		; // ignore spaces

	ungetc(c, stdin);

	if (!isdigit(c) && c != '-' && c != '.') {
		// data read is not a number
		fsetpos(stdin, &pos);
		fprintf(stderr, "Error: input mismatch, expected long.\n");
		exit(1);
	}

	long l;
	fscanf(stdin, "%ld", &l);
	return l;
}

/* in_read_short:  returns the next short value from "stdin"
 *
 *  Returns:
 *      short -- the short read
 */
short in_read_short()
{
	if (!in_has_next()) {
		fprintf(stderr, "Error: there is no short to read.\n");
		exit(1);
	}

	fpos_t pos;
	fgetpos(stdin, &pos);
	int c;

	while ((c = getchar()) && isspace(c))
		; // ignore spaces

	ungetc(c, stdin);

	if (!isdigit(c) && c != '-' && c != '.') {
		// data read is not a number
		fsetpos(stdin, &pos);
		fprintf(stderr, "Error: input mismatch, expected short.\n");
		exit(1);
	}

	short s;
	fscanf(stdin, "%hd", &s);
	return s;
}

/* in_read_all_strings:  parses out all the individual string tokens from "stdin" and returns
 *      them in an array
 *
 *  Returns:
 *      char ** -- the array of strings read
 */
char **in_read_all_strings()
{
	if (!in_has_next()) {
		fprintf(stderr, "Error: there are no strings to read.\n");
		exit(1);
	}

	char *array[1000], **ap;
	int i = 0;

	while (in_has_next()) {
		array[i++] = in_read_string();
	}

	array[i] = NULL; // terminate the array
	ap = array;
	return ap;
}

/* in_read_all_lines:  reads all lines from "stdin" as strings into an array
 *
 *  Returns:
 *      char ** -- the array of lines read
 */
char **in_read_all_lines()
{
	if (!in_has_next()) {
		fprintf(stderr, "Error: there are no lines to read.\n");
		exit(1);
	}

	char *array[1000], **ap;
	int i = 0;

	while (in_has_next()) {
		array[i++] = in_read_line();
	}

	array[i] = NULL; // terminate the array
	ap = array;
	return ap;
}

#endif
