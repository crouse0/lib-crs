#include "../in.h"
#include "Stack.h"

// tests for Stack.h
int main(int argc, char const *argv[])
{
	char *inp;
	int d;
	Stack *s = Stack_init();

	while (!in_is_empty()) {
		inp = in_read_string();

		if (strcmp(inp, "*") == 0) {
			printf("%d ", Stack_pop(s));
		} else {
			d = atoi(inp);
			Stack_push(s, d);
		}

		free(inp);
		inp = NULL;
	}

	printf("\n");
	Stack_free(s);
	return 0;
}