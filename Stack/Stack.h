#include <stdio.h>
#include <stdlib.h>

#ifndef STACK_H
#define STACK_H

typedef struct StackNode {
	int data;
	struct StackNode *next;
} StackNode;

typedef struct Stack {
	StackNode *first;
	int n;
} Stack;

/* StackNode_init:  allocates memory for a new StackNode
 *
 *  Arguments:
 *      int data -- integer to embed in the StackNode
 *
 *  Returns:
 *      StackNode * -- a pointer to a new StackNode
 */
StackNode *StackNode_init(int data)
{
	StackNode *n;

	if ((n = (StackNode *)malloc(sizeof(StackNode))) == NULL) {
		return NULL;
	}

	n->data = data;
	return n;
}

/* Stack_init:  allcates memory for a new Stack
 *
 *  Returns:
 *      Stack * -- a pointer to a new Stack
 */
Stack *Stack_init()
{
	Stack *l;

	if ((l = (Stack *)malloc(sizeof(Stack))) == NULL) {
		return NULL;
	}

	l->first = NULL;
	l->n = 0;
	return l;
}

/* Stack_push:  adds the given integer to the given Stack
 *
 *  Arguments:
 *      Stack *s -- a pointer to a Stack
 *      int item -- integer to push onto the Stack
 */
void Stack_push(Stack *s, int item)
{
	StackNode *n;

	if ((n = StackNode_init(item)) == NULL) {
		return;
	}

	n->next = s->first;
	s->first = n;
	s->n++;
}

/* Stack_pop:  removes and returns the most recently added item on the Stack
 *
 *  Arguments:
 *      Stack *s -- a pointer to a Stack
 *
 *  Returns:
 *      int -- the integer popped from the stack
 */
int Stack_pop(Stack *s)
{
	if (s->n == 0) {
		// stack must not be empty
		fprintf(stderr, "Error: stack must not be empty.\n");
		exit(1);
	}

	int item = s->first->data;
	StackNode *tmp = s->first;
	s->first = s->first->next;
	free(tmp);
	tmp = NULL;
	s->n--;
	return item;
}

/* Stack_free:  frees all allocated resources for a given Stack
 *
 *  Arguments:
 *      Stack *s -- a pointer to a Stack
 */
void Stack_free(Stack *s)
{
	StackNode *itr;

	while ((itr = s->first) != NULL) {
		s->first = s->first->next;
		free(itr);
		itr = NULL;
	}

	free(s);
	s = NULL;
}

#endif