#include <stdio.h>
#include <stdlib.h>

#include "../Bag/Bag.h"
#include "../Stack/Stack.h"

#ifndef Digraph_H
#define Digraph_H

typedef struct Digraph {
	int V;         // number of vertices
	int E;         // number of edges in the Digraph
	Bag **adj;     // singly-linked-list of adjacent vertices
	int *indegree; // indegree[i] is number of vertice heads adjacent to vertex i

} Digraph;

/* Digraph_init:  allocates and initializes memory for a new Digraph
 *
 *  Arguments:
 *      int V -- number of vertices
 *
 *  Returns:
 *      Digraph * -- the new Digraph
 */
Digraph *Digraph_init(int V)
{
	Digraph *d;
	int i;

	if ((d = (Digraph *)malloc(sizeof(Digraph))) == NULL) {
		// couldn't allocate the requested memory
		fprintf(stderr, "Error: invalid call to malloc().\n");
		exit(1);
	}

	if ((d->adj = (Bag **)malloc(sizeof(Bag *) * V)) == NULL) {
		fprintf(stderr, "Error: invalid call to malloc().\n");
		exit(1);
	}

	if ((d->indegree = (int *)malloc(sizeof(int) * V)) == NULL) {
		fprintf(stderr, "Error: invalid call to malloc().\n");
		exit(1);
	}

	for (i = 0; i < V; i++) {
		// initialize the adjacency lists and indegree array
		d->adj[i] = Bag_init();
		d->indegree[i] = 0;
	}

	d->V = V;
	d->E = 0;
	return d;
}

// validates that the given vertex "v" is in "d"
static void validateVertex(Digraph *d, int v)
{
	if (v < 0 || v >= d->V) {
		// "v" is invalid
		fprintf(stderr, "Error: vertex is not between 0 and %d.\n", d->V - 1);
		exit(1);
	}
}

/* Digraph_add_edge:  adds an edge between "v" and "w"
 *
 *  Arguments:
 *      Digraph *d -- a pointer to a Digraph
 *      int v -- the first vertice
 *      int w -- the second vertice
 */
void Digraph_add_edge(Digraph *d, int v, int w)
{
	validateVertex(d, v), validateVertex(d, w);
	d->E++;
	d->indegree[w]++;
	Bag_add(d->adj[v], w);
}

/* Digraph_file_init:  creates a new Digraph then inserts the data from the
 *  given file into the Digraph
 *
 *  Arguments:
 *      FILE *fp -- a pointer to a file
 *
 *  Returns:
 *      Digraph * -- the new Digraph
 */
Digraph *Digraph_file_init(FILE *fp)
{
	validateFile(fp);
	int V, E, i, v, w;
	V = fin_read_int(fp);
	E = fin_read_int(fp);

	if (V < 0 || E < 0) {
		fprintf(stderr, "Error: invalid number of vertices or edges.\n");
		exit(1);
	}

	Digraph *d = Digraph_init(V);

	while (fin_has_next(fp)) {
		v = fin_read_int(fp);
		w = fin_read_int(fp);
		validateVertex(d, v);
		validateVertex(d, w);
		Digraph_add_edge(d, v, w);
	}

	return d;
}

/* Digraph_vertex_outdegree:  determines the number of directed edges incident from "v".
 *  Otherwise knows as the outdegree of vertex "v"
 *
 *  Arguments:
 *      Digraph *d -- a pointer to a Digraph
 *      int v -- the source vertex
 *
 *  Returns:
 *      int -- the outdegree of "v"
 */
int Digraph_vertex_outdegree(Digraph *d, int v)
{
	validateVertex(d, v);
	return d->adj[v]->n;
}

/* Digraph_vertex_indegree:  determines the number of directed edges incident to "v".
 *  Otherwise knows as the indegree of vertex "v"
 *
 *  Arguments:
 *      Digraph *d -- a pointer to a Digraph
 *      int v -- the source vertex
 *
 *  Returns:
 *      int -- the indegree of "v"
 */
int Digraph_vertex_indegree(Digraph *d, int v)
{
	validateVertex(d, v);
	return d->indegree[v];
}

/* Digraph_reverse:  returns a reverse of the given digraph
 *
 *  Arguments:
 *      Digraph *d -- a pointer to a Digraph
 *
 *  Returns:
 *      The reverse of "d"
 */
Digraph *Digraph_reverse(Digraph *d)
{
	Digraph *reverse = Digraph_init(d->V);
	int i;

	for (i = 0; i < d->V; i++) {
		BagNode *p;

		for (p = d->adj[i]->first; p != NULL; p = p->next) {
			Digraph_add_edge(reverse, i, p->item);
		}
	}

	return reverse;
}

/* Digraph_print:  outputs a formatted representation of the given Digraph
 *
 *  Arguments:
 *      Digraph *d -- a pointer to a Digraph
 */
void Digraph_print(Digraph *d)
{
	printf("%d vertices and %d edges.\n", d->V, d->E);
	int i;

	for (i = 0; i < d->V; i++) {
		printf("%d..%d..%d: ", i, Digraph_vertex_indegree(d, i), Digraph_vertex_outdegree(d, i));
		Bag_print(d->adj[i]);
		printf("\n");
	}
}

/* Digraph_free:  frees all allocated resources for a given Digraph
 *
 *  Arguments:
 *      Digraph *d -- a pointer to a Digraph
 */
void Digraph_free(Digraph *d)
{
	int i;

	for (i = 0; i < d->V; i++) {
		Bag_free(d->adj[i]);
		d->adj[i] = NULL;
	}

	free(d->adj);
	d->adj = NULL;
	free(d->indegree);
	d->indegree = NULL;
	free(d);
	d = NULL;
}
#endif