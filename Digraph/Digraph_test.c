#include "../in.h"
#include "../fin.h"

#include "Digraph.h"

int main(int argc, char const *argv[])
{
	Digraph *d = Digraph_file_init(stdin);
	Digraph_print(d);
	Digraph_free(d);
	return 0;
}