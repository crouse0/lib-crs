#include "../in.h"
#include "QuickUnionUF.h"

// tests for QuickUnionUF.h
int main(int argc, char const *argv[])
{
	int N = in_read_int();
	QuickUnionUF *qu = QuickUnionUF_init(N);

	while (in_has_next()) {
		int p = in_read_int();
		int q = in_read_int();
		printf("%d %d\n", p, q);

		if (QuickUnionUF_connected(qu, p, q)) {
			continue;
		}

		QuickUnionUF_union(qu, p, q);
	}

	printf("%d components.\n", qu->count);
	QuickUnionUF_free(qu);
	return 0;
}