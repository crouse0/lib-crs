#include <stdio.h>
#include <stdlib.h>

#ifndef QUICKUNIONUF_H
#define QUICKUNIONUF_H

typedef struct QuickUnionUF {
	int *parent; // parent[i] = parent of i
	int count;   // number of components
	int N;       // number of indices in parent
} QuickUnionUF;

/* QuickUnionUF_init:  allocates memory and initializes a new QuickUnionUF
 *
 *  Arguments:
 *      int N -- the initial number of components
 *
 *  Returns:
 *      QuickUnionUF * -- a pointer to a new QuickUnionUF
 */
QuickUnionUF *QuickUnionUF_init(int N)
{
	QuickUnionUF *qu;
	int i;

	if ((qu = (QuickUnionUF *)malloc(sizeof(QuickUnionUF))) == NULL || (qu->parent = (int *)malloc(sizeof(int) * N)) == NULL) {
		// mallocs must not fail
		fprintf(stderr, "Error: invalid malloc().\n");
		return NULL;
	}

	for (i = 0; i < N; i++) {
		qu->parent[i] = i;
	}

	qu->count = N;
	qu->N = N;
	return qu;
}

/* QuickUnionUF_validate:  determines if "p" is a valid vertex
 *
 *  Arguments:
 *      QuickUnionUF *qu -- a pointer to a QuickUnionUF
 *      int p -- the site to validate
 */
void QuickUnionUF_validate(QuickUnionUF *qu, int p)
{
	int N = qu->N;

	if (p < 0 || p >= N) {
		fprintf(stderr, "Error: index %d is not between 0 and %d.\n", p, N - 1);
		exit(1);
	}
}

/* QuickUnionUF_find:  determines the component identifier for site "p"
 *
 *  Arguments:
 *      QuickUnionUF *qu -- a pointer to a QuickUnionUF
 *      int p -- the site
 *
 *  Returns:
 *      int -- the component for the site
 */
int QuickUnionUF_find(QuickUnionUF *qu, int p)
{
	QuickUnionUF_validate(qu, p);

	while (p != qu->parent[p]) {
		p = qu->parent[p];
	}

	return p;
}

/* QuickUnionUF_connected:  determines if two sites are connected
 *
 *  Arguments:
 *      QuickUnionUF *qu -- a pointer to a QuickUnionUF
 *      int p -- the first site
 *      int q -- the second site
 *
 *  Returns:
 *      int -- (1) if the sites are connected and (0) if the sites are not connected
 */
int QuickUnionUF_connected(QuickUnionUF *qu, int p, int q)
{
	return (QuickUnionUF_find(qu, p) == QuickUnionUF_find(qu, q)) ? 1 : 0;
}

/* QuickUnionUF_union:  connects two sites if they are not already connected
 *
 *  Arguments:
 *      QuickUnionUF *qu -- a pointer to a QuickUnionUF
 *      int p -- the first site
 *      int q -- the second site
 */
void QuickUnionUF_union(QuickUnionUF *qu, int p, int q)
{
	int rootP = QuickUnionUF_find(qu, p);
	int rootQ = QuickUnionUF_find(qu, q);

	if (rootP == rootQ) {
		return;
	}

	qu->parent[rootP] = rootQ;
	qu->count--;
}

/* QuickUnionUF_free:  frees all allocated resources for the given QuickUnionUF
 *
 *  Arguments:
 *      QuickUnionUF *qu -- a pointer to a QuickUnionUF
 */
void QuickUnionUF_free(QuickUnionUF *qu)
{
	if (qu->parent != NULL) {
		free(qu->parent);
	}

	qu->parent = NULL;
	free(qu);
	qu = NULL;
}

#endif