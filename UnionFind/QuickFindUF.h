#include <stdio.h>
#include <stdlib.h>

#ifndef QUICKFINDUF_H
#define QUICKFINDUF_H

typedef struct QuickFindUF {
	int *id;   // id[i] = component identifier of i
	int count; // number of components
	int N;     // length of id
} QuickFindUF;

/* QuickFindUF_init:  allocates memory for and initializes a new QuickFindUF struct pointer
 *
 *  Arguments:
 *      int N -- initial number of components
 *
 *  Returns:
 *      QuickFindUF * -- a pointer to a new QuickFindUF
 */
QuickFindUF *QuickFindUF_init(int N)
{
	int i;
	QuickFindUF *q;

	if ((q = (QuickFindUF *)malloc(sizeof(QuickFindUF))) == NULL || (q->id = (int *)malloc(sizeof(int) * N)) == NULL) {
		// memory was not allocated correctly
		fprintf(stderr, "Warning: invalid malloc().\n");
		return NULL;
	}

	for (i = 0; i < N; i++) {
		// assign id to each element
		q->id[i] = i;
	}

	q->count = N;
	q->N = N;
	return q;
}

/* QuickFindUF_validate:  validates that "p" is a valid index in "q"
 *
 *  Arguments:
 *
 *      QuickFindUF *qf -- a pointer to a QuickFindUF
 *      int p -- the index to validate
 */
void QuickFindUF_validate(QuickFindUF *qf, int p)
{
	int N = qf->N;

	if (p < 0 || p >= N) {
		fprintf(stderr, "Error: index %d is not between 0 and %d.\n", p, N - 1);
		exit(1);
	}
}

/* QuickFindUF_find:  determines the component identifier for the given site
 *
 *  Arguments:
 *      QuickFindUF *qf -- a pointer to a QuickFindUF
 *      int p -- the site to check
 *
 *  Returns:
 *      int -- the component identifier of "p"
 */
int QuickFindUF_find(QuickFindUF *qf, int p)
{
	QuickFindUF_validate(qf, p);
	return qf->id[p];
}

/* QuickFindUF_connected:  determines if two sites are connected
 *
 *  Arguments:
 *      QuickFindUF *qf -- a pointer to a QuickFindUF
 *      int q -- the first site
 *      int f -- the seconde site
 *
 *  Returns:
 *      int -- (1) if the sites are connected and (0) if they are not
 */
int QuickFindUF_connected(QuickFindUF *qf, int p, int q)
{
	QuickFindUF_validate(qf, q);
	QuickFindUF_validate(qf, q);
	return qf->id[p] == qf->id[q] ? 1 : 0;
}
/* QuickFindUF_union:  connects two sites to have the same components
 *
 *  Arguments:
 *      QuickFindUF *qf -- a pointer to a QuickFindUF
 *      int p -- the first site
 *      int q -- the second site
 */
void QuickFindUF_union(QuickFindUF *qf, int p, int q)
{
	int pID, qID, i;
	QuickFindUF_validate(qf, p);
	QuickFindUF_validate(qf, q);
	pID = qf->id[p], qID = qf->id[q]; // needed to reduce number of array accesses

	if (pID == qID) {
		// components are already connected
		return;
	}

	for (i = 0; i < qf->count; i++) {
		// update connections
		qf->id[i] = qf->id[i] == pID ? qID : qf->id[i];
	}

	qf->count--;
}

/* QuickFindUF_free:  frees all allocated resources for a given QuickFindUF
 *
 *  Arguments:
 *      QuickFindUF *qf -- a pointer to a QuickFindUF
 */
void QuickFindUF_free(QuickFindUF *qf)
{
	if (qf->id != NULL) {
		free(qf->id);
	}

	qf->id = NULL;
	free(qf);
	qf = NULL;
}

#endif