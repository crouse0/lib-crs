#include "../in.h"
#include "QuickFindUF.h"

// tests for QuickFindUF.h
int main(int argc, char *argv[])
{
	int n = in_read_int();
	QuickFindUF *qf = QuickFindUF_init(n);

	while (in_has_next()) {
		int p = in_read_int();
		int q = in_read_int();
		QuickFindUF_validate(qf, p);
		QuickFindUF_validate(qf, q);
		printf("%d %d\n", p, q);

		if (QuickFindUF_connected(qf, p, q)) {
			continue;
		}

		QuickFindUF_union(qf, p, q);
	}

	printf("%d components\n", qf->count);
	QuickFindUF_free(qf);
	return 0;
}
