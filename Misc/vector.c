#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

/* vector:  struct for a vector */
typedef struct vector {
	int d;		  // size of vector
	double *data; // vector values
} vector;

/* print_vector:  prints a formated vector */
void print_vector(vector *v)
{
	int i;

	for (i = 0; i < v->d; i++) {
		printf("[%.2f]\n", v->data[i]);
	}

	printf("\n");
}

/* init_vector:  initializes and allocates memory for a new vector
 *  - d size of the vector
 *  - data: the components to put in the vector
 */
vector *init_vector(int d, double data[])
{
	vector *v;

	if ((v = (vector *)malloc(sizeof(vector))) == NULL) {
		return NULL; // mem allocation failed
	}

	v->d = d;
	v->data = (double *)malloc(sizeof(double) * d);
	int i;

	for (i = 0; i < d; i++) {
		v->data[i] = data[i];
	}

	return v;
}

/* dimmension:  returns the dimmension of v */
int dim(vector *v)
{
	return v->d;
}

/* diff:  returns a vector with components as the difference of v and w */
vector *diff(vector *v, vector *w)
{
	assert(dim(v) == dim(w)); // dimmensions must agree
	double d[dim(v)];
	int i;

	for (i = 0; i < dim(v); i++) {
		d[i] = v->data[i] - w->data[i];
	}

	vector *c = init_vector(dim(v), d);
	return c;
}

/* sum:  returns a vector with components as the sum of v and w */
vector *sum(vector *v, vector *w)
{
	assert(dim(v) == dim(w)); // dimmensions must agree
	double d[dim(v)];
	int i;

	for (i = 0; i < dim(v); i++) {
		d[i] = v->data[i] + w->data[i];
	}

	vector *c = init_vector(dim(v), d);
	return c;
}

/* scale:  returns a vector with every componenet in v scaled by alpha */
vector *scale(vector *v, double alpha)
{
	vector *c = init_vector(v->d, v->data);
	assert(c != NULL);
	int i;

	for (i = 0; i < dim(v); i++) {
		c->data[i] *= alpha;
	}

	return c;
}

/* dot:  returns the dot product of v and w */
double dot(vector *v, vector *w)
{
	assert(dim(v) == dim(w)); // dimmensions must agree
	double sum = 0.0;
	int i;

	for (i = 0; i < v->d; i++) {
		sum = sum + (v->data[i] * w->data[i]);
	}

	return sum;
}

/* mag:  returns the magnitude of v */
double mag(vector *v)
{
	return sqrt(dot(v, v));
}

/* distTo:  returns the euclidean distance between v and w */
double distTo(vector *v, vector *w)
{
	assert(dim(v) == dim(w)); // dimmensions must agree
	return mag(diff(v, w));
}

/* direction:  returns a unit vector in the direction of v */
vector *direction(vector *v)
{
	assert(mag(v) != 0.0);
	return scale(v, 1.0 / mag(v));
}

/* main:  tests for vector
 *  - argc the argument count
 *  - argv the command line arguments
 */
int main(int argc, char const *argv[])
{
	double x[4] = {1.0, 2.0, 3.0, 4.0};
	double y[4] = {5.0, 2.0, 4.0, 1.0};
	vector *v = init_vector(4, x);
	vector *w = init_vector(4, y);
	print_vector(v);
	print_vector(w);
	vector *z = sum(v, w);
	print_vector(z);
	z = scale(z, 10);
	print_vector(z);
	printf("|v| = %.2f\n", mag(v));
	printf("<v, w> = %.2f\n", dot(v, w));
	printf("dist(v, w) = %.2f\n", distTo(v, w));
	printf("dir(v) = \n");
	print_vector(direction(v));
	return 0;
}