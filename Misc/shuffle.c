#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* print_ints:  prints a formated int array */
void print_ints(int *a, int N)
{
	int i;

	for (i = 0; i < N; i++) {
		printf("%d%s", a[i], (i != N - 1) ? ", " : "\n");
	}
}

/* print_chars:  prints a formated char array */
void print_chars(char *a, int N)
{
	int i;

	for (i = 0; i < N; i++) {
		printf("%c%s", a[i], (i != N - 1) ? ", " : "\n");
	}
}

/* swapi:  swaps two integers in a generic array
 *  - a array to make the swap in
 *  - i, j: indices in a to swap
 */
void swapi(void *a, int i, int j)
{
	int *nums = (int *)a;
	int t = nums[i];
	nums[i] = nums[j];
	nums[j] = t;
}

/* swapc:  swaps two chars in a generic array
 *  - a array to make the swap in
 *  - i, j: indices in a to swap
 */
void swapc(void *a, int i, int j)
{
	char *chars = (char *)a;
	char t = chars[i];
	chars[i] = chars[j];
	chars[j] = t;
}

/* shuffle:  Knuth shuffle
 *  - a array to shuffle
 *  - N length of a
 *  - swap pointer to a swapping function
 */
void shuffle(void *a, int N, void (*swap)(void *, int, int))
{
	int i;

	for (i = 0; i < N; i++) {
		int r = (int)rand() % (i + 1);
		swap(a, i, r);
	}
}

/* main:
 *  - argc argument count
 *  - argv the command line arguments
 */
int main(int argc, char const *argv[])
{
	srand(time(NULL));
	int nums[5] = {1, 2, 3, 4, 5};
	char chars[5] = "ABCDE";
	shuffle((void *)nums, 5, swapi);
	shuffle((void *)chars, 4, swapc);
	print_ints(nums, 5);
	print_chars(chars, 5);
	return 0;
}