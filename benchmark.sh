#!/bin/bash
#./benchmark.sh <input-file> <test-count> <-s/l/a>

rm *.class
javac -cp .:stdlib.jar Test.java
make clean
make
clear

echo "input file: \"$1\""

echo
echo "java benchmark..."
time -p {
    for ((i = 0; i < $2; i++))
    {
        java -cp .:stdlib.jar Test $3 < $1 > java.out
    }
}

echo
echo "C stdin benchmark..."
time -p {
    for ((i = 0; i < $2; i++))
    {
        ./in_test $3 < $1 > c.out
    }
}

echo
echo "C file i/o benchmark..."
time -p {
    for ((i = 0; i < $2; i++))
    {
        ./fin_test $3 $1 > cf.out
    }
}

diff c.out java.out > out1
diff cf.out java.out > out2
diff cf.out c.out > out3

rm java.out c.out cf.out
make clean
