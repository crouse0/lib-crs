all: in fin java

in: in_test.c in.h
	gcc -g -o in_test in_test.c

fin: fin_test.c fin.h
	gcc -g -o fin_test fin_test.c

java: Test.java
	javac -cp .:stdlib.jar Test.java

clean:
	rm -rf in_test fin_test *.dSYM Test.class