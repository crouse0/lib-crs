#include "fin.h"

int main(int argc, char *argv[])
{
	if (argc < 3) {
		fprintf(stderr, "Usage: %s [-s/-l/-a] <file-name>\n", *argv);
		exit(1);
	}

	FILE *fp = fopen(argv[2], "r");
	char *option = argv[1];

	if (!strcmp(option, "-s")) {
		while (fin_has_next(fp)) {
			printf("|%s|\n", fin_read_string(fp));
		}
	} else if (!strcmp(option, "-l")) {
		while (fin_has_next(fp)) {
			printf("|%s|\n", fin_read_line(fp));
		}
	} else if (!strcmp(option, "-a")) {
		printf("%s", fin_read_all(fp));
	} else {
		fprintf(stderr, "Error: invalid flag.\n");
		exit(1);
	}

	fclose(fp);
	return 0;
}
