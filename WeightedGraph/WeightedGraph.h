#include <stdio.h>
#include <stdlib.h>

#include "Bag_Edge.h"

#ifndef WEIGHTEDGRAPH_H
#define WEIGHTEDGRAPH_H

typedef struct WeightedGraph {
	int V;          // number of vertices
	int E;          // number of edges in the graph
	Bag_Edge **adj; // singly-linked-list of adjacent vertices
} WeightedGraph;

/* WeightedGraph_init:  allocates and initializes memory for a new WeightedGraph
 *
 *  Arguments:
 *      int V -- number of vertices
 *
 *  Returns:
 *      WeightedGraph * -- the new WeightedGraph
 */
WeightedGraph *WeightedGraph_init(int V)
{
	WeightedGraph *wg;
	int i;

	if ((wg = (WeightedGraph *)malloc(sizeof(WeightedGraph))) == NULL) {
		// couldn't allocate the requested memory
		fprintf(stderr, "Error: invalid call to malloc().\n");
		exit(1);
	}

	if ((wg->adj = (Bag_Edge **)malloc(sizeof(Bag_Edge *) * V)) == NULL) {
		fprintf(stderr, "Error: invalid call to malloc().\n");
		exit(1);
	}

	for (i = 0; i < V; i++) {
		// initialize the adjacency lists
		wg->adj[i] = Bag_Edge_init();
	}

	wg->V = V;
	wg->E = 0;
	return wg;
}

// validates that the given vertex "v" is in "g"
static void validateVertex(WeightedGraph *wg, int v)
{
	if (v < 0 || v >= wg->V) {
		// "v" is invalid
		fprintf(stderr, "Error: vertex is not between 0 and %d.\n", wg->V - 1);
		exit(1);
	}
}

/* WeightedGraph_add_edge:  adds an edge between "v" and "w"
 *
 *  Arguments:
 *      WeightedGraph *wg -- a pointer to a WeightedGraph
 *      int v -- the first vertice
 *      int w -- the second vertice
 *      float weight -- the weight of the edge
 */
void WeightedGraph_add_edge(WeightedGraph *wg, int v, int w, float weight)
{
	validateVertex(wg, w), validateVertex(wg, v);
	wg->E++;
	Bag_Edge_add(wg->adj[v], v, w, weight);
	Bag_Edge_add(wg->adj[w], w, v, weight);
}

/* WeightedGraph_file_init:  creates a new WeightedGraph then inserts the data from the
 *  given file into the WeightedGraph
 *
 *  Arguments:
 *      FILE *fp -- a pointer to a file
 *
 *  Returns:
 *      WeightedGraph * -- the new graph
 */
WeightedGraph *WeightedGraph_file_init(FILE *fp)
{
	validateFile(fp);
	int V, E, i, v, w;
	float weight;
	V = fin_read_int(fp);
	E = fin_read_int(fp);

	if (V < 0 || E < 0) {
		fprintf(stderr, "Error: invalid number of vertices or edges.\n");
		exit(1);
	}

	WeightedGraph *wg = WeightedGraph_init(V);

	while (fin_has_next(fp)) {
		printf("%f\n", fin_read_float(fp));
		// v = fin_read_int(fp);
		// w = fin_read_int(fp);
		// weight = fin_read_float(fp);
		// validateVertex(wg, v);
		// validateVertex(wg, w);
		// WeightedGraph_add_edge(wg, v, w, weight);
	}

	return wg;
}

/* WeightedGraph_vertex_degree:  determines the number of vertices incident on "v"
 *
 *  Arguments:
 *      WeightedGraph *wg -- a pointer to a WeightedGraph
 *      int v -- the vertice
 *
 *  Returns:
 *      int -- the number of vertices connected to "v"
 */
int WeightedGraph_vertex_degree(WeightedGraph *wg, int v)
{
	validateVertex(wg, v);
	return wg->adj[v]->n;
}

/* WeightedGraph_print:  outputs a formatted representation of the given WeightedGraph
 *
 *  Arguments:
 *      WeightedGraph *wg -- a pointer to a WeightedGraph
 */
void WeightedGraph_print(WeightedGraph *wg)
{
	printf("%d vertices and %d edges.\n", wg->V, wg->E);
	int i;

	for (i = 0; i < wg->V; i++) {
		printf("%d: ", i);
		Bag_Edge_print(wg->adj[i]);
		printf("\n");
	}
}

/* WeightedGraph_free:  frees all allocated resources for a given WeightedGraph
 *
 *  Arguments:
 *      WeightedGraph *wg -- a pointer to a WeightedGraph
 */
void WeightedGraph_free(WeightedGraph *wg)
{
	int i;

	for (i = 0; i < wg->V; i++) {
		Bag_Edge_free(wg->adj[i]);
		wg->adj[i] = NULL;
	}

	free(wg->adj);
	wg->adj = NULL;
	free(wg);
	wg = NULL;
}

#endif