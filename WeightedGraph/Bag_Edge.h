#include <stdio.h>
#include <stdlib.h>

#ifndef BAG_EDGE_H
#define BAG_EDGE_H

/* Edge */
typedef struct Edge {
	int v;
	int w;
	float weight;
} Edge;

/* AdjList */
typedef struct Bag_EdgeNode {
	Edge item;
	struct Bag_EdgeNode *next;
} Bag_EdgeNode;

typedef struct Bag_Edge {
	Bag_EdgeNode *first;
	int n;
} Bag_Edge;

// allocates and initializes a new Bag_Edge Node
static Bag_EdgeNode *Bag_EdgeNode_init(int v, int w, float weight)
{
	Bag_EdgeNode *n;

	if ((n = (Bag_EdgeNode *)malloc(sizeof(Bag_EdgeNode))) == NULL) {
		fprintf(stderr, "Warning: invalid malloc().\n");
		return NULL;
	}

	n->item.v = v;
	n->item.w = w;
	n->item.weight = weight;
	n->next = NULL;
	return n;
}

/* Bag_Edge_init:  allocates and initializes a new Bag_Edge
 *
 *  Returns:
 *      Bag_Edge * -- the new Bag_Edge
 */
Bag_Edge *Bag_Edge_init()
{
	Bag_Edge *b;

	if ((b = (Bag_Edge *)malloc(sizeof(Bag_Edge))) == NULL) {
		fprintf(stderr, "Warning: invalid malloc().\n");
		return NULL;
	}

	b->first = NULL;
	b->n = 0;
	return b;
}

/* Bag_Edge_add:  adds a new item to the Bag_Edge
 *
 *  Arguments:
 *      Bag_Edge *b -- a pointer to a Bag_Edge
 *      int item -- integer to add to the Bag_Edge
 */
void Bag_Edge_add(Bag_Edge *b, int v, int w, float weight)
{
	Bag_EdgeNode *n = Bag_EdgeNode_init(v, w, weight);
	n->next = b->first;
	b->first = n;
	b->n++;
}

/* Bag_Edge_print:  iterates through a Bag_Edge
 *
 *  Arguments:
 *      Bag_Edge *b -- a pointer to a Bag_Edge
 */
void Bag_Edge_print(Bag_Edge *b)
{
	Bag_EdgeNode *n;

	for (n = b->first; n != NULL; n = n->next) {
		printf("%d|%.3f%s", n->item.w, n->item.weight, n->next == NULL ? "" : ", ");
	}
}

/* Bag_Edge_free:  frees all allocated resources for a given Bag_Edge
 *
 *  Arguments:
 *      Bag_Edge *b -- a pointer to a Bag_Edge
 */
void Bag_Edge_free(Bag_Edge *b)
{
	Bag_EdgeNode *itr;

	while ((itr = b->first) != NULL) {
		b->first = b->first->next;
		free(itr);
		itr = NULL;
	}

	free(b);
}

#endif