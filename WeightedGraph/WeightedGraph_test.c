#include "../in.h"
#include "../fin.h"
#include "WeightedGraph.h"

int main(int argc, char const *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Usage: %s <file-name>\n", *argv);
		exit(1);
	}

	FILE *fp = fopen(argv[1], "r");
	validateFile(fp);
	// printf("%d", fin_read_int(fp));
	WeightedGraph *wg = WeightedGraph_file_init(fp);
	// WeightedGraph_print(wg);
	// WeightedGraph_free(wg);
	fclose(fp);
	return 0;
}