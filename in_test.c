#include "in.h"
#include <string.h>

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Usage: %s [-s/-l/-a]\n", *argv);
		exit(1);
	}

	char *option = argv[1];

	if (!strcmp(option, "-s")) {
		while (in_has_next()) {
			printf("|%s|\n", in_read_string());
		}
	} else if (!strcmp(option, "-l")) {
		while (in_has_next()) {
			printf("|%s|\n", in_read_line());
		}
	} else if (!strcmp(option, "-a")) {
		printf("%s", in_read_all());
	} else {
		fprintf(stderr, "Error: invalid flag.\n");
		exit(1);
	}

	return 0;
}
