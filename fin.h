#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#ifndef FIN_H
#define FIN_H

// determines if a given file is valid
static void validateFile(FILE *fp)
{
	if (fp == NULL) {
		fprintf(stderr, "Error: invalid FILE.\n");
		exit(1);
	}
}

/* fin_is_empty:  determines if the given file is empty
 *
 *  Arguments:
 *      FILE *fp -- a pointer to a File
 *
 *  Returns:
 *      int -- (1) the File is empty or (0) the File is not empty
 */
int fin_is_empty(FILE *fp)
{
	validateFile(fp);
	int c;
	fpos_t pos;
	fgetpos(fp, &pos);

	while ((c = fgetc(fp)) && isspace(c))
		; // ignore whitespaces

	ungetc(c, fp);
	fsetpos(fp, &pos);
	return c == EOF ? 1 : 0;
}

/* fin_has_next:  determines if a given file has more data
 *
 *  Arguments:
 *      FILE *fp -- a pointer to a File
 *
 *  Returns:
 *      int -- (1) the File has more data or (0) the File is empty
 */
int fin_has_next(FILE *fp)
{
	validateFile(fp);
	int c;
	fpos_t pos;
	fgetpos(fp, &pos);

	while ((c = fgetc(fp)) && isspace(c))
		; // ignore whitespaces

	ungetc(c, fp);
	fsetpos(fp, &pos);
	return c == EOF ? 0 : 1;
}

/* fin_read_line:  reads and returns a line from a file as a string
 *
 *  Arguments:
 *      FILE *fp -- a pointer to a File
 *
 *  Returns:
 *      char * -- a pointer to the line read
 */
char *fin_read_line(FILE *fp)
{
	size_t MAXSIZE = 1000, len;
	char *lp = NULL;

	if ((len = getline(&lp, &MAXSIZE, fp)) == 0) {
		fprintf(stderr, "Error: there is no line to read.\n");
		exit(1);
	}

	if (lp[len - 1] == '\n') {
		lp[len - 1] = 0;
	}

	return lp;
}

/* fin_read_all:  reads the entire file into a string
 *
 *  Arguments:
 *      FILE *fp -- a pointer to a file
 *
 *  Returns:
 *      char * -- a pointer to the stream of input read
 */
char *fin_read_all(FILE *fp)
{
	validateFile(fp);

	if (!fin_has_next(fp)) {
		fprintf(stderr, "Error: there is nothing to read.\n");
		exit(1);
	}

	const size_t MAXSIZE = 7000000;
	char all[MAXSIZE], *ap;
	int c, i = 0;

	while ((c = fgetc(fp)) != EOF) {
		all[i++] = c;

		if (i == MAXSIZE - 1) {
			break;
		}
	}

	all[i] = 0;
	ap = all;
	return ap;
}

/* fin_read_string:  returns the next string of text, using whitespaces
 *  as a delimiter
 *
 *  Arguments:
 *      FILE *fp -- a pointer to a file
 *
 *  Returns:
 *      char * -- a pointer to the string of input read
 */
char *fin_read_string(FILE *fp)
{
	validateFile(fp);

	if (!fin_has_next(fp)) {
		fprintf(stderr, "Error: there is not a string to read.\n");
		exit(1);
	}

	const size_t MAXSIZE = 1000;
	char s[MAXSIZE], *sp;
	fscanf(fp, "%999s", s);
	sp = s;
	return sp;
}

/* fin_read_int:  returns the next integer from "fp"
 *
 *  Arguments:
 *      FILE *fp -- a pointer to a file
 *
 *  Returns:
 *      int -- the integer read
 */
int fin_read_int(FILE *fp)
{
	validateFile(fp);

	if (!fin_has_next(fp)) {
		fprintf(stderr, "Error: there is not an int to read.\n");
		exit(1);
	}

	fpos_t pos;
	fgetpos(fp, &pos);
	int c;

	while ((c = fgetc(fp)) && isspace(c))
		; // ignore spaces

	ungetc(c, fp);

	if (!isdigit(c) && c != '-' && c != '.') {
		// data read is not a number
		fsetpos(fp, &pos);
		fprintf(stderr, "Error: input mismatch, expected integer.\n");
		exit(1);
	}

	fscanf(fp, "%d", &c); // read number into c
	return c;
}

/* fin_read_float:  returns the next floating point value from "fp"
 *
 *  Arguments:
 *      FILE *fp -- a pointer to a file
 *
 *  Returns:
 *      float -- the floating point read
 */
float fin_read_float(FILE *fp)
{
	validateFile(fp);

	if (!fin_has_next(fp)) {
		fprintf(stderr, "Error: there is not a float to read.\n");
		exit(1);
	}

	fpos_t pos;
	fgetpos(fp, &pos);
	int c;

	while ((c = fgetc(fp)) && isspace(c))
		; // ignore spaces

	ungetc(c, fp);

	if (!isdigit(c) && c != '-' && c != '.') {
		// data read is not a number
		fsetpos(fp, &pos);
		fprintf(stderr, "Error: input mismatch, expected float.\n");
		exit(1);
	}

	float f;
	fscanf(fp, "%f", &f);
	return f;
}

/* fin_read_double:  returns the next double precision floating point value from "fp"
 *
 *  Arguments:
 *      FILE *fp -- a pointer to a file
 *
 *  Returns:
 *      double -- the double read
 */
double fin_read_double(FILE *fp)
{
	validateFile(fp);

	if (!fin_has_next(fp)) {
		fprintf(stderr, "Error: there is not a double to read.\n");
		exit(1);
	}

	fpos_t pos;
	fgetpos(fp, &pos);
	int c;

	while ((c = fgetc(fp)) && isspace(c))
		; // ignore spaces

	ungetc(c, fp);

	if (!isdigit(c) && c != '-' && c != '.') {
		// data read is not a number
		fsetpos(fp, &pos);
		fprintf(stderr, "Error: input mismatch, expected double.\n");
		exit(1);
	}

	double d;
	fscanf(fp, "%lf", &d);
	return d;
}

/* fin_read_long:  returns the next long value from "fp"
 *
 *  Arguments:
 *      FILE *fp -- a pointer to a file
 *
 *  Returns:
 *      long -- the long read
 */
long fin_read_long(FILE *fp)
{
	validateFile(fp);

	if (!fin_has_next(fp)) {
		fprintf(stderr, "Error: there is not a long to read.\n");
		exit(1);
	}

	fpos_t pos;
	fgetpos(fp, &pos);
	int c;

	while ((c = fgetc(fp)) && isspace(c))
		; // ignore spaces

	ungetc(c, fp);

	if (!isdigit(c) && c != '-' && c != '.') {
		// data read is not a number
		fsetpos(fp, &pos);
		fprintf(stderr, "Error: input mismatch, expected long.\n");
		exit(1);
	}

	long l;
	fscanf(fp, "%ld", &l);
	return l;
}

/* fin_read_short:  returns the next short value from "fp"
 *
 *  Arguments:
 *      FILE *fp -- a pointer to a file
 *
 *  Returns:
 *      short -- the short read
 */
short fin_read_short(FILE *fp)
{
	validateFile(fp);

	if (!fin_has_next(fp)) {
		fprintf(stderr, "Error: there is not a short to read.\n");
		exit(1);
	}

	fpos_t pos;
	fgetpos(fp, &pos);
	int c;

	while ((c = fgetc(fp)) && isspace(c))
		; // ignore spaces

	ungetc(c, fp);

	if (!isdigit(c) && c != '-' && c != '.') {
		// data read is not a number
		fsetpos(fp, &pos);
		fprintf(stderr, "Error: input mismatch, expected short.\n");
		exit(1);
	}

	short s;
	fscanf(fp, "%hd", &s);
	return s;
}

/* fin_read_all_strings:  parses out all the individual string tokens from "fp" and returns
 *      them in an array
 *
 *  Arguments:
 *      FILE *fp -- a pointer to a file
 *
 *  Returns:
 *      char ** -- the array of strings read
 */
char **fin_read_all_strings(FILE *fp)
{
	validateFile(fp);

	if (!fin_has_next(fp)) {
		fprintf(stderr, "Error: there are no strings to read.\n");
		exit(1);
	}

	char *array[1000];
	int i = 0;

	while (fin_has_next(fp)) {
		array[i++] = fin_read_string(fp);
	}

	array[i] = NULL; // terminate the array
	char **ret = array;
	return ret;
}

/* fin_read_all_lines:  reads all lines from "fp" as strings into an array
 *
 *  Arguments:
 *      FILE *fp -- a pointer to a file
 *
 *  Returns:
 *      char ** -- the array of lines read
 */
char **fin_read_all_lines(FILE *fp)
{
	validateFile(fp);

	if (!fin_has_next(fp)) {
		fprintf(stderr, "Error: there are no lines to read.\n");
		exit(1);
	}

	size_t MAXSIZE = 1000;
	char *array[1000], *line = NULL, **ap;
	int i = 0, len = 0;

	while ((len = getline(&line, &MAXSIZE, fp)) >= 0) {
		*(line + len - 1) = 0;
		array[i++] = line;
	}

	ap = array;
	return ap;
}

#endif