#include <stdio.h>
#include <stdlib.h>

#ifndef QUEUE_H
#define QUEUE_H

typedef struct QueueNode {
	int item;
	struct QueueNode *next;
} QueueNode;

typedef struct Queue {
	QueueNode *first, *last;
	int n;
} Queue;

/* QueueNode init:  allocates memory and initializes a new QueueNode
 *
 *  Arguments:
 *      int item -- integer to embed in the Node
 *
 *  Returns:
 *      QueueNode * -- a pointer to a new QueueNode
 */
static QueueNode *QueueNode_init(int item)
{
	QueueNode *n;

	if ((n = (QueueNode *)malloc(sizeof(QueueNode))) == NULL) {
		// malloc failed
		return NULL;
	}

	n->item = item;
	n->next = NULL;
	return n;
}

/* Queue_init:  allocates memory and initializes a new linked queue
 *
 *  Returns:
 *      Queue * -- a pointer to a new Queue
 */
Queue *Queue_init()
{
	Queue *lq;

	if ((lq = (Queue *)malloc(sizeof(Queue))) == NULL) {
		// malloc failed
		return NULL;
	}

	lq->first = lq->last = NULL;
	lq->n = 0;
	return lq;
}

/* Queue_init:  allocates and enques a new item on the given Queue
 *
 *  Arguments:
 *      Queue *q -- a pointer to a Queue
 *      int item -- integer to add to the Queue
 */
void Queue_enqueue(Queue *q, int item)
{
	QueueNode *n = QueueNode_init(item);

	if (q->first == NULL) {
		// queue is empty
		q->last = n;
		q->first = q->last;
	} else {
		q->last->next = n;
		q->last = n;
	}

	q->n++;
}

/* Queue_dequeue:  deques the least recently added item from the given Queue
 *
 *  Arguments:
 *      Queue *q -- a pointer to a Queue
 *
 *  Returns:
 *      int -- the integer that was dequeued
 */
int Queue_dequeue(Queue *q)
{
	if (q->first == NULL) {
		// queue must be populated
		fprintf(stderr, "Error: queue is empty.\n");
		exit(1);
	}

	int item = q->first->item;
	QueueNode *tmp = q->first->next;
	free(q->first);
	q->first = NULL;
	q->first = tmp;
	q->n--;

	if (q->first == NULL) {
		q->last = NULL;
	}

	return item;
}

/* Queue_print:  dequeues and outputs every element from the queue
 *
 *  Arguments:
 *      Queue *q -- a pointer to a Queue
 */
void Queue_print(Queue *q)
{
	while (q->first) {
		printf("%d ", Queue_dequeue(q));
	}

	printf("\n");
}

/* Queue_free:  frees all allocated resources for a given Queue
 *
 *  Arguments:
 *      Queue *q -- a pointer to a Queue
 */
void Queue_free(Queue *q)
{
	QueueNode *t;

	while (q->first != NULL) {
		t = q->first;
		q->first = q->first->next;
		free(t);
	}

	q->last = NULL;
	free(q);
	q = NULL;
}

#endif