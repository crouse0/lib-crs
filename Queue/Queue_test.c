#include "../in.h"
#include "Queue.h"

// tests for Queue.h
int main(int argc, char const *argv[])
{
	Queue *q = Queue_init();

	while (in_has_next()) {
		int item = in_read_int();
		Queue_enqueue(q, item);
	}

	Queue_print(q);
	Queue_free(q);
	return 0;
}